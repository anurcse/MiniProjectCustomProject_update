-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 19, 2016 at 07:06 PM
-- Server version: 5.6.14
-- PHP Version: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `phonebook`
--

-- --------------------------------------------------------

--
-- Table structure for table `phonenumber`
--

CREATE TABLE IF NOT EXISTS `phonenumber` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `number` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

--
-- Dumping data for table `phonenumber`
--

INSERT INTO `phonenumber` (`id`, `name`, `number`, `email`) VALUES
(31, 'A', '*566#', 'anurcse@yahoo.com'),
(32, 'Ashikur rahman', '01915989911', 'anurcse@yahoo.com'),
(33, 'Saddam', '01719118833', 'anurcse@yahoo.com');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `country` text NOT NULL,
  `city` text NOT NULL,
  `mobile` text NOT NULL,
  `photo` text NOT NULL,
  `gender` text NOT NULL,
  `termsofservice` text NOT NULL,
  `dateofbirth` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `password`, `country`, `city`, `mobile`, `photo`, `gender`, `termsofservice`, `dateofbirth`) VALUES
(18, 'ALIFNUR', 'anurcse@yahoo.com', '1', 'Bangladesh', 'Dhaka', '01719111883', 'vlcsnap-2015-07-09-19h44m24s112.png', 'Male', 'Yes', '2016-01-13'),
(19, 'Ashikur rahman', 'nasir@lankabangla.com', '1', 'Bangladesh', 'Dhaka', '01712365425', 'vlcsnap-2015-07-09-19h44m24s112.png', 'Male', 'Yes', '2016-01-12'),
(20, 'Ashikur rahman2', 'nasir2@lankabangla.com', '2', 'Bangladesh', 'Dhaka', '01712365425', 'vlcsnap-2015-07-09-19h44m24s112.png', 'Male', 'Yes', '2016-01-12'),
(21, 'rasma Akter', 'neilresa@gmail.com', '1234', 'Afghanistan', 'Kabul', '01676859138', 'texstitchbd.com.png', 'Female', 'Yes', '1993-01-06'),
(22, 'khaled mahmud', 'kalie@yahoo.com', '12', 'Bangladesh', 'Dinajpur', '01234587', 'fariiwala.com.png', 'Male', 'Yes', '2016-01-05'),
(23, 'Mohammed Nasir Uddin Chowdhury', 'a@yahoo.com', '123', 'Bangladesh', 'Dhaka', '01234587', 'banglaabritticorcakendro.com.png', 'Male', 'Yes', '2016-01-06');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
