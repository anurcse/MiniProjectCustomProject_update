<?php
session_start();


if(isset($_SESSION['email'])){
    


?>

<?php include_once '../includes/hader.php'; ?>

<?php
include_once '../models/ShowAllNumber.php';


$show = new ShowAllNumber();
$shows = $show->Allnumber();
?>

<a href="pdf.php" style="float: right;" class="btn btn-info">PDF &nbsp;<span class="glyphicon glyphicon-download-alt"></span></a>
<a href="download-xlsx.php" class="btn btn-info" style="float: right;">Excel &nbsp;<span class="glyphicon glyphicon-download-alt"></span> </a> 

<table class="table table-bordered table-responsive">
    
    <tr >
        <td >Sl</td>
        <td >Name</td>
        <td>Number</td>
        <td>Action</td>
    </tr>
    <?php
    $slno = 1;
    foreach ($shows as $all) {
        ?>
        <tr>
            <td><?php echo $slno; ?></td>
            <td><?php echo $all->name;?> </td>
            <td><?php echo $all->number;?></td>
            <td>
                <a href="editnumber.php?id=<?php echo $all->id;?>" class="btn btn-warning"><span class="glyphicon glyphicon-pencil"></span> &nbsp; Edit</a> 
                <a class="number btn btn-danger" href="deletenumber.php?id=<?php echo $all->id;?>" > <span class="glyphicon glyphicon-remove"></span> &nbsp; Delete</a>
            </td>
        </tr>
        <?php
        $slno ++;
    }
        ?>
    </table>





    <?php include_once '../includes/footer.php'; ?>     


<?php } 


else{
    echo "<script>window.open('index.php','_self')</script>";
}
?>