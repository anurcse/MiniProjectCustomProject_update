<?php

session_start();

?>
<html>
    <head>
        <title>Mini Project Registration</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="../resources/css/bootstrap.min.css">
        <link rel="stylesheet" href="../resources/css/style.css">

        <!-- Optional theme -->
        <link rel="stylesheet" href="../resources/css/bootstrap-theme.min.css">

        <!-- Latest compiled and minified JavaScript -->
        <script src="../resources/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div>
            <div class="header">


            </div>

            <div class="reg">
                <div class="col-xs-2">
                    <ul class="nav nav-pills nav-stacked">
                        <li > <a href="addnumber.php">Add Number</a></li>
                        <li><a href="viewnumber.php">View All Number</a></li>
                        <li><a href="profile.php">View User Profile</a> </li>          
                        <li><a href="changepassword.php">Change Password</a></li>
                        <li><a href="edit.php" >Edit User Account</a>
                        <li> <a href="deleteuseraccount.php">Delete User Account </a></li>

                        <li><a href="logout.php">[<?php echo $_SESSION['email']; ?>] Logout</a></li>
                    </ul>
                </div>



                <div class="col-xs-10">



                </div>


            </div>
        </div>
        <footer>

        </footer>

    </body>
</html>