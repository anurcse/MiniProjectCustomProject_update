<?php
session_start();


if(isset($_SESSION['email'])){
    


?>
<html>
    <head>
        <title>Mini Project Registration</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="../resources/css/bootstrap.min.css">
        <link rel="stylesheet" href="../resources/css/style.css">

        <!-- Optional theme -->
        <link rel="stylesheet" href="../resources/css/bootstrap-theme.min.css">

        <!-- Latest compiled and minified JavaScript -->
        <script src="../resources/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div>
            <div class="header">
                <a href="logout.php" class="btn btn-primary">[<?php echo $_SESSION['email']; ?>] Logout</a>

            </div>

            <div class="reg">
                <div class="col-xs-2">
                    <ul class="nav nav-pills nav-stacked">
                        <li > <a href="addnumber.php">Add Number</a></li>
                        <li><a href="viewnumber.php">View All Number</a></li>
                        <li><a href="profile.php">View User Profile</a> </li> 
                        <li><a href="changepassword.php">Change Password</a></li>
                        <li><a href="edituser.php" >Edit User Account</a>

                        <li> <a href="deleteuseraccount.php">Delete User Account </a></li>

                        <li><a href="logout.php">[<?php echo $_SESSION['email']; ?>] Logout</a>
                    </ul>
                </div>


                <div class="col-xs-10">



                    <form action="store.php" method="POST">
                        <label>Name</label>
                        <input type="text" name="name" >
                        <label>Phone Number:</label>
                        <input type="text" name="phonenumber">

                        <input type="hidden" name="email" value="<?php echo $_SESSION['email']; ?>">

                        <input type="submit" name="submit" value="Save Number">
                        <!-- $user = $_SESSION['customer_email']; -->

                    </form>

                    <?php include_once '../includes/footer.php'; ?>     

<?php } 


else{
    echo "<script>window.open('index.php','_self')</script>";
}
?>